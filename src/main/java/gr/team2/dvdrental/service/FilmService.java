package gr.team2.dvdrental.service;

import gr.team2.dvdrental.model.Film;
import gr.team2.dvdrental.dtos.FilmDTO;

import java.util.List;

public interface FilmService {
    List<Film> parseJsonFile(String filename);
    void addNewFilm(Film film);
    List<FilmDTO> viewAllFilms();
    Film viewFilmByTitle(String title);
}
