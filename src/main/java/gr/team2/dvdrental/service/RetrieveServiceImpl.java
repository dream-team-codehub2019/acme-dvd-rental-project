package gr.team2.dvdrental.service;

import gr.team2.dvdrental.model.CustomerAccount;
import gr.team2.dvdrental.model.Film;
import gr.team2.dvdrental.model.Rental;
import gr.team2.dvdrental.repository.CustomerAccountsRepository;
import gr.team2.dvdrental.repository.FilmRepository;
import gr.team2.dvdrental.repository.RentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RetrieveServiceImpl implements RetrieveService {
    @Autowired
    private RentsRepository rentsRepository;

    @Autowired
    private CustomerAccountsRepository customerAccountsRepository;

    @Autowired
    private FilmRepository filmRepository;

    /**
     * Method fetchBestSelling Films gets as
     * @param allTheFilms from film repository database and
     * @return a sorted list of best selling films limited by 10
     */

    @Override
    public List<Film> fetchBestSellingFilms(List<Film> allTheFilms) {
        List<Film> sortedFilmsByTimesRent = filmRepository.findAll();
        sortedFilmsByTimesRent.sort(Comparator.comparing(Film::getTimesRent));
        return sortedFilmsByTimesRent.stream().limit(10).collect(Collectors.toList());
    }

    /**
     * Method fetchOutOfStockList gets as
     * @param allTheRentals from rental repository database and
     * @return a list of rented copies
     */

    @Override
    public List<Rental> fetchOutOfStockList(List<Rental> allTheRentals) {
        List<Rental> outOfStockList = rentsRepository.findAll();
        for (Rental rental : rentsRepository.findAll()){
            if(rental.getCopyID().isRent() == true){
                outOfStockList.add(rental);
            }
        }
        if(outOfStockList == null){
            System.out.println("All DVD copies are available");
        }
        return outOfStockList;
    }

    /**
     * Method fetchTopTwentyCustomers gets as
     * @param allTheCustomers from CustomerAccount repository database and
     * @return a sorted list of top Customers limited by 20
     */

    @Override
    public List<CustomerAccount> fetchTopTwentyCustomers(List<CustomerAccount> allTheCustomers) {
        List<CustomerAccount> topTwentyCustomers = customerAccountsRepository.findAll();
        topTwentyCustomers.sort(Comparator.comparing((CustomerAccount::getMoneySpent)));
        return topTwentyCustomers.stream().limit(20).collect(Collectors.toList());
    }

    /**
     * Method fetchWeeklyRentals gets as
     * @param allTheRentals from rental repository database and
     * @return a list of Weekly Rentals
     */

    @Override
    public List<Rental> fetchWeeklyRentals(List<Rental> allTheRentals) {
        List<Rental> weeklyRentals = rentsRepository.findAll();
        LocalDate today = LocalDate.now();
        for (Rental rental : rentsRepository.findAll()){
            if (ChronoUnit.DAYS.between(rental.getRentalDate(),today) <= 7){
                weeklyRentals.add(rental);
            }
        }
        if (weeklyRentals == null){
            System.out.println("No list exists");
        }
        return weeklyRentals;
    }

    /**
     * Method fetchMonthlyRentals gets as
     * @param allTheRentals from rental repository database and
     * @return a list of Monthly Rentals
     */

    @Override
    public List<Rental> fetchMonthlyRentals(List<Rental> allTheRentals){
        List<Rental> monthlyRentals = rentsRepository.findAll();
        LocalDate today = LocalDate.now();
        for (Rental rental : rentsRepository.findAll()){
            if (ChronoUnit.DAYS.between(rental.getRentalDate(),today) <= 30){
                monthlyRentals.add(rental);
            }
        }
        if (monthlyRentals == null){
            System.out.println("No list exists");
        }
        return monthlyRentals;
    }

}

