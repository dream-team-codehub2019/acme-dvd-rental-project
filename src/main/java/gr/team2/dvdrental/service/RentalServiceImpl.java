package gr.team2.dvdrental.service;

import gr.team2.dvdrental.model.*;
import gr.team2.dvdrental.repository.CopiesRepository;
import gr.team2.dvdrental.repository.PaymentRepository;
import gr.team2.dvdrental.repository.RentsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Service
@Slf4j
public class RentalServiceImpl implements RentalService {

    @Autowired
    private RentsRepository rentsRepository;

    @Autowired
    private CopiesRepository copiesRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    /**
     * Handles a new rental.
     * @param copyID
     * @param customerAccount
     */
    public void makeRental(long copyID, CustomerAccount customerAccount) {
        //here we communicate with the database (with the list for now)
        //we check if there are available copies of given Film
        //return message to User
        //if yes, copies-- and create new Rental
        //else, returns message "Not available copies"
        Rental rental = new Rental();
        try {
            LocalDate today = LocalDate.now();
            for (Copies copy : copiesRepository.findAll()) {
                if (copy.getFilmID().getFilmID() == copyID && copy.isRent() == false) {
                    rental.setCustomerAccountID(customerAccount);
                    rental.setCopyID(copy);
                    rental.setRentalDate(today);
                    copy.setRent(true);
                    rentsRepository.save(rental);
                    copiesRepository.save(rental.getCopyID());
                    break;
                } else {
                    log.info("Not available copies");
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method has double functionality:
     * 1: searches for films that the given string is contained in the title
     * 2:prints the film that match with the available copies, their types and IDs.
     * @param filmID
     */
    public void isCopyAvailable(long filmID){
        for (Copies copy : copiesRepository.findAll()) {
            if (copy.getFilmID().getFilmID()==filmID && copy.isRent() == false) {
                log.info("Film: "+copy.getFilmID().getTitle()+" is available, TYPE: "+copy.getType()+" CopyId "+copy.getCopyID());
            }else if(copy.getFilmID().getFilmID()==filmID && copy.isRent() == true){
                log.info("Film: "+copy.getFilmID().getTitle()+" not available, TYPE: "+copy.getType()+" CopyId "+copy.getCopyID());
            }
        }
    }

    /**
     * Handles the return of a rental.
     * @param rental
     */
    public void makeReturn(Rental rental) {
        Payment payment = new Payment();
        LocalDate today = LocalDate.now();
        payment.setPaymentDate(today);
        rental.setReturnDate(today);
        rental.setPaymentID(payment);
        payment.setPrice(fullPriceOfRentalBasedOnDaysRented(rental));
        rental.getCopyID().setRent(false);
        rentsRepository.save(rental);
        paymentRepository.save(rental.getPaymentID());
        copiesRepository.save(rental.getCopyID());
    }

    /**
     * Calculates the final price of the rental based on the days the customers kept the film.
     * @param rental
     * @return
     */
    public double fullPriceOfRentalBasedOnDaysRented(Rental rental){
        double finalprice = 0;
        long daysRented = ChronoUnit.DAYS.between(LocalDate.now(), rental.getRentalDate());
        if (rental.getCopyID().isRent()==false){
            System.out.println("Film is not rented and can't be priced.");
        }else{
            if(rental.getCopyID().getType()==TypeOfCopy.DVD){
                //DVD PRICE CALCULATION
                if(daysRented <= 1){
                    finalprice = 0.5;
                }else if(daysRented > 1 && daysRented < 10){
                    finalprice = 0.5 + (daysRented-1)*0.25;
                }else if(daysRented >= 10 && daysRented < 20){
                    finalprice = 0.5 + (daysRented-1)*0.25 + 10;//Penalty for keeping the copy more than 9 days
                }else{
                    finalprice = 0.5 + (daysRented-1)*0.5 + 20;//Penalty for keeping the copy more than 19 days
                }
            }else{
                //BLU-RAY PRICE CALCULATION
                if(daysRented <= 1){
                    finalprice = 1;
                }else if(daysRented > 1 && daysRented < 10){
                    finalprice = 1 + (daysRented-1)*0.5;
                }else if(daysRented >= 10 && daysRented < 20){
                    finalprice = 1 + (daysRented-1)*0.5 + 15;//Penalty for keeping the copy more than 9 days
                }else{
                    finalprice = 1 + (daysRented-1)*0.5 + 25;//Penalty for keeping the copy more than 19 days
                }
            }
        }
        return finalprice;
    }
}
