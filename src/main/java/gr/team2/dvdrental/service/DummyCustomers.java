package gr.team2.dvdrental.service;

import gr.team2.dvdrental.model.CustomerAccount;
import gr.team2.dvdrental.repository.CustomerAccountsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
public class DummyCustomers implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private CustomerAccountsRepository customerAccountsRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        CustomerAccount mitsos = new CustomerAccount();
        mitsos.setIdNumber("1");
        mitsos.setPhoneNumber(6666);
        mitsos.setFirstName("mitsos");
        mitsos.setLastName("mos");
        mitsos.setAddress("pouthena1");
        mitsos.setEmail("mitsos.com");
        CustomerAccount thanos = new CustomerAccount();
        thanos.setIdNumber("2");
        thanos.setPhoneNumber(6667);
        thanos.setFirstName("thanos");
        thanos.setLastName("nos");
        thanos.setAddress("pouthena2");
        thanos.setEmail("thanos.com");
        CustomerAccount maria = new CustomerAccount();
        maria.setIdNumber("3");
        maria.setPhoneNumber(6688);
        maria.setFirstName("maria");
        maria.setLastName("ria");
        maria.setAddress("pouthena3");
        maria.setEmail("maria.com");
        CustomerAccount giota = new CustomerAccount();
        giota.setIdNumber("4");
        giota.setPhoneNumber(7799);
        giota.setFirstName("giota");
        giota.setLastName("ota");
        giota.setAddress("pouthena4");
        giota.setEmail("giota.com");
        CustomerAccount nikos = new CustomerAccount();
        nikos.setIdNumber("5");
        nikos.setPhoneNumber(4563);
        nikos.setFirstName("nikos");
        nikos.setLastName("kos");
        nikos.setAddress("pouthena5");
        nikos.setEmail("nikos.com");
        CustomerAccount lena = new CustomerAccount();
        lena.setIdNumber("6");
        lena.setPhoneNumber(9671);
        lena.setFirstName("lena");
        lena.setLastName("ena");
        lena.setAddress("pouthena6");
        lena.setEmail("lena.com");
        customerAccountsRepository.save(mitsos);
        customerAccountsRepository.save(thanos);
        customerAccountsRepository.save(maria);
        customerAccountsRepository.save(giota);
        customerAccountsRepository.save(nikos);
        customerAccountsRepository.save(lena);

    }
}
