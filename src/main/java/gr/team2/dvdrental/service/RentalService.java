package gr.team2.dvdrental.service;

import gr.team2.dvdrental.model.CustomerAccount;
import gr.team2.dvdrental.model.Rental;

public interface RentalService {

    void makeRental(long copyID, CustomerAccount customerAccount);

    void makeReturn(Rental rental);
}
