package gr.team2.dvdrental.service;

import gr.team2.dvdrental.model.CustomerAccount;
import gr.team2.dvdrental.repository.CustomerAccountsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AccountManagerImpl implements AccountManager {
    @Autowired
    private CustomerAccountsRepository customerAccountsRepository;

    @Override
    public long saveCustomerAccount(CustomerAccount customerAccount) {
        try{
            customerAccountsRepository.save(customerAccount);
        }catch (Exception ee){
            ee.printStackTrace();
        }
        return customerAccount.getCustomerAccountID();
    }

    @Override
    public boolean customerExists(long id) {
        return customerAccountsRepository.existsById(id);
    }

    @Override
    public List<CustomerAccount> getAllCustomers() {
        return customerAccountsRepository.findAll();
    }

    public void createCustomerAccount(CustomerAccount customer) {
        try{
            customerAccountsRepository.save(customer);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void editCustomerAccount(CustomerAccount account) {
        if (accountExists(account)) {
            //here we edit an existing account
            //and update the repository
        } else {
            //throw message that account doesn't exist
        }
    }

    public void deleteCustomerAccount(CustomerAccount customerAccount) {
        if (accountExists(customerAccount)) {
            customerAccountsRepository.delete(customerAccount);
        } else {
            //throw message that account doesn't exist
        }
    }

    public boolean accountExists(CustomerAccount account) {
        //customerAccountsRepository.findOne(gr.team2.dvdrental.model.CustomerAccount);
        return true;
    }

    public CustomerAccount getCustomerByFirstName(String firstName){
        return customerAccountsRepository.findCustomerAccountByFirstName(firstName);
    }


}
