package gr.team2.dvdrental.service;

import gr.team2.dvdrental.model.CustomerAccount;
import gr.team2.dvdrental.model.Film;
import gr.team2.dvdrental.model.Rental;

import java.util.List;

public interface RetrieveService {
    List<Film> fetchBestSellingFilms(List<Film> allTheFilms);

    List<Rental> fetchOutOfStockList(List<Rental> allTheRentals);

    List<CustomerAccount> fetchTopTwentyCustomers(List<CustomerAccount> allTheCustomers);

    List<Rental> fetchWeeklyRentals(List<Rental> allTheRentals);

    List<Rental> fetchMonthlyRentals(List<Rental> allTheRentals);

}
