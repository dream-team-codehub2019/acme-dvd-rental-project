package gr.team2.dvdrental.service;

import gr.team2.dvdrental.model.CustomerAccount;

import java.util.List;

public interface AccountManager {
    void createCustomerAccount(CustomerAccount customer);

    void editCustomerAccount(CustomerAccount account);

    void deleteCustomerAccount(CustomerAccount account);

    CustomerAccount getCustomerByFirstName(String firstName);

    List<CustomerAccount> getAllCustomers();

    long saveCustomerAccount(CustomerAccount customerAccount);

    boolean customerExists(long id);
}
