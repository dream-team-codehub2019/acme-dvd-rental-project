package gr.team2.dvdrental.service;


import gr.team2.dvdrental.model.Actor;
import gr.team2.dvdrental.model.Film;

import gr.team2.dvdrental.model.FilmCategory;
import gr.team2.dvdrental.dtos.FilmDTO;
import gr.team2.dvdrental.repository.ActorsRepository;
import gr.team2.dvdrental.repository.CategoryRepository;
import gr.team2.dvdrental.repository.FilmRepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class FilmServiceImpl implements FilmService{

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ActorsRepository actorsRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    public List<Film> parseJsonFile(String filename) {

        List<Film> filmList = new ArrayList<>();
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(filename);

            JSONTokener tokener = new JSONTokener(stream);
            JSONArray films = new JSONArray(tokener);

            for (int i = 0; i < films.length(); i++){
                Film film = new Film();
                JSONObject filmObject = films.getJSONObject(i);

                //Set title
                String title = filmObject.getString("movieTitle");
                film.setTitle(title);

                //Set category
                List<FilmCategory> filmCategories = new ArrayList<>();
                String[] categories = filmObject.getString("genre").split("[|]");
                for (int c = 0; c < categories.length; c ++){
                    FilmCategory category = new FilmCategory();
                    category.setCategoryName(categories[c]);
                    switch (category.getCategoryName()){
                        case "Drama":
                            category.setCategoryId(1);
                            break;
                        case "Romance":
                            category.setCategoryId(2);
                            break;
                        case "Adventure":
                            category.setCategoryId(3);
                            break;
                        case "Children":
                            category.setCategoryId(4);
                            break;
                        case "Comedy":
                            category.setCategoryId(5);
                            break;
                        case "Fantasy":
                            category.setCategoryId(6);
                            break;
                        case "Horror":
                            category.setCategoryId(7);
                            break;
                        case "Thriller":
                            category.setCategoryId(8);
                            break;
                        case "Mystery":
                            category.setCategoryId(9);
                            break;
                        default:
                            category.setCategoryId(10);
                    }

                    category = categoryRepository.saveAndFlush(category);
                    filmCategories.add(category);
                }
                film.setCategoryList(filmCategories);

                //Set director
                String director = filmObject.getString("director");
                film.setDirector(director);

                //Set Year
                int year = Integer.parseInt(filmObject.getString("year").split("[/]")[2]);
                film.setProductionYear(year);

                //Set description
                String description = filmObject.getString("description");
                film.setDescription(description);

                //Set actors
                JSONArray actors = filmObject.getJSONArray("actor");
                List<Actor> filmActors = new ArrayList<>();
                for (int a = 0; a < actors.length(); a++){
                    JSONObject jsonActor = actors.getJSONObject(a);
                    String actorName = jsonActor.getString("firstName");
                    String actorLastName = jsonActor.getString("lastName");
                    Actor actor = new Actor();
                    actor.setFirstName(actorName);
                    actor.setLastName(actorLastName);

                    actor = actorsRepository.save(actor);
                    filmActors.add(actor);
                }
                film.setActorsList(filmActors);

                film = filmRepository.save(film);
                filmList.add(film);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filmList;
    }


    @Override
    public void addNewFilm(Film film) {  }

    public List<FilmDTO> viewAllFilms(){
        List<FilmDTO> filmDTOList = new ArrayList<>();
        List<Film> filmsInDB = filmRepository.findAll();
        log.debug("LIST BEFORE DTO :{}", filmsInDB.toString() );
        for(Film filmInDB : filmsInDB){
            FilmDTO filmDTO = new FilmDTO();
            filmDTO.setFilmName(filmInDB.getTitle());
            log.debug("CATEGORY LIST {} FOR FILM : {}", filmInDB.getCategoryList(), filmInDB.getFilmID());
            filmDTO.setCategory(filmInDB.getCategoryList());
            filmDTO.setActorsList(filmDTO.getActorsList());
            filmDTO.setDescription(filmDTO.getDescription());
            filmDTO.setDirector(filmDTO.getDirector());
            filmDTO.setProductionYear(filmDTO.getProductionYear());
            filmDTO.setCopies(filmDTO.getCopies());
            filmDTOList.add(filmDTO);
        }
        return filmDTOList;
    }

    public Film viewFilmByTitle(String title){
        for (Film f:filmRepository.findAll()){
            if(f.getTitle()==title)
                return f;
        }
        return null;
    }

    public Film viewFilmById(long id){
        for (Film f:filmRepository.findAll()){
            if(f.getFilmID()==id)
                return f;
        }
        return null;
    }
}
