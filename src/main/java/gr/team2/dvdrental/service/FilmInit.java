//package gr.team2.dvdrental.service;
//
//import gr.team2.dvdrental.model.Actor;
//import gr.team2.dvdrental.model.Film;
//import gr.team2.dvdrental.model.FilmCategory;
//import gr.team2.dvdrental.repository.ActorsRepository;
//import gr.team2.dvdrental.repository.CategoryRepository;
//import gr.team2.dvdrental.repository.FilmRepository;
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.json.JSONTokener;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class FilmInit {
//    @Autowired
//    FilmRepository filmRepository;
//    @Autowired
//    CategoryRepository categoryRepository;
//    @Autowired
//    ActorsRepository actorsRepository;
//
//    public List<Film> parseJsonFile(String filename) {
//
//        List<Film> filmList = new ArrayList<>();
//        FileInputStream stream = null;
//        try {
//            stream = new FileInputStream(filename);
//
//            JSONTokener tokener = new JSONTokener(stream);
//            JSONArray films = new JSONArray(tokener);
//
//            for (int i = 0; i < films.length(); i++){
//                Film film = new Film();
//                JSONObject filmObject = films.getJSONObject(i);
//
//                //Set title
//                String title = filmObject.getString("movieTitle");
//                film.setTitle(title);
//
//                //Set category
//                List<FilmCategory> filmCategories = new ArrayList<>();
//                String[] categories = filmObject.getString("genre").split("[|]");
//                for (int j = 0; j < categories.length; j ++){
//                    FilmCategory category = new FilmCategory();
//                    category.setCategoryName(categories[j]);
//                    filmCategories.add(category);
//                    categoryRepository.save(category);
//                }
//                film.setCategoryList(filmCategories);
//
//                //Set director
//                String director = filmObject.getString("director");
//                film.setDirector(director);
//
//                //Set Year
//                int year = Integer.parseInt(filmObject.getString("year").split("[/]")[2]);
//                film.setProductionYear(year);
//
//                //Set description
//                String description = filmObject.getString("description");
//                film.setDescription(description);
//
//                //Set actors
//                JSONArray actors = filmObject.getJSONArray("actor");
//                List<Actor> filmActors = new ArrayList<>();
//                for (int j = 0; j < actors.length(); j++){
//                    JSONObject jsonActor = actors.getJSONObject(j);
//                    String actorName = jsonActor.getString("firstName");
//                    String actorLastName = jsonActor.getString("lastName");
//                    Actor actor = new Actor();
//                    actor.setFirstName(actorName);
//                    actor.setLastName(actorLastName);
//                    filmActors.add(actor);
//                    actorsRepository.save(actor);
//                }
//                film.setActorsList(filmActors);
//
//                filmList.add(film);
//                filmRepository.save(film);
//            }
//
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        return filmList;
//    }
//}
