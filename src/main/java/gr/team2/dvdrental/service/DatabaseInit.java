package gr.team2.dvdrental.service;


import gr.team2.dvdrental.model.Actor;
import gr.team2.dvdrental.model.Film;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class DatabaseInit implements CommandLineRunner {

    @Autowired
    private FilmService filmService;

    @Override
    public void run(String... args) throws Exception {
        List<Film> films = filmService.parseJsonFile("MOCK_DATA.json");
    }
}