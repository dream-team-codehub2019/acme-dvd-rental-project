package gr.team2.dvdrental.dtos;

import gr.team2.dvdrental.model.Actor;
import gr.team2.dvdrental.model.Copies;
import gr.team2.dvdrental.model.FilmCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO{
    private String filmName;
    private List<FilmCategory> category;
    private List<Actor> actorsList;
    private List<Copies> copies;
    private String description;
    private String director;
    private int productionYear;
}
