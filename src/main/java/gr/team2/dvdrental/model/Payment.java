package gr.team2.dvdrental.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.MODULE)
    private long paymentID;

    @Column(nullable = false)
    private double price;
    @Column
    private LocalDate paymentDate;

    @OneToOne(optional = false)
    @JoinColumn(name="rentalID")
    private Rental rentalID;

}
