package gr.team2.dvdrental.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Rental {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private long rentalId;

    @Column(nullable = false)
    private LocalDate rentalDate;
    @Column
    private LocalDate returnDate;

    @OneToOne(optional = false, mappedBy = "rentalID", targetEntity = Copies.class)
    private Copies copyID;

    @OneToOne(optional = false, mappedBy = "rentalID", targetEntity = Payment.class)
    @JoinColumn(name = "paymentID")
    private Payment paymentID;

    @ManyToOne(optional = false)
    @JoinColumn(name="customerAccountID",referencedColumnName = "customerAccountID")
    private CustomerAccount customerAccountID;

}
