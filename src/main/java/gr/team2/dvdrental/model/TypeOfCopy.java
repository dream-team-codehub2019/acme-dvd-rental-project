package gr.team2.dvdrental.model;

public enum TypeOfCopy {
    DVD, BLU_RAY
}
