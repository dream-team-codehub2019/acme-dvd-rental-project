package gr.team2.dvdrental.model;

public enum CustomerQuality {
    GOOD, NOT_BAD, BAD, DISASTER
}
