package gr.team2.dvdrental.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilmCategory {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long categoryId;

    @Column
    private String categoryName;


    @ManyToMany(mappedBy="categoryList", fetch = FetchType.EAGER)
    private List<Film> filmList;

    @Override
    public String toString() {
        return "FilmCategory{" +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
