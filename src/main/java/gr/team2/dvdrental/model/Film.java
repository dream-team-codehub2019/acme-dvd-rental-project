package gr.team2.dvdrental.model;

import lombok.*;
import net.minidev.json.annotate.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@Setter(AccessLevel.NONE)
    private long filmID;

    @Column(nullable = false, length = 128)
    private String title;

    @Column(length = 512)
    private String description;

    @Column(length = 64)
    private String director;

    @Column (length = 32)
    private int productionYear;

    @Column
    @OneToMany(mappedBy="filmID",targetEntity=Copies.class)
    private List<Copies> copies;

    @Column
    private int timesRent;


    @ManyToMany
    @JoinTable(name="Film_Actors", joinColumns=
    @JoinColumn(name="filmID", referencedColumnName="filmID"), inverseJoinColumns=
    @JoinColumn(name="actorID", referencedColumnName="actorID") )
    private List<Actor> actorsList;

    @ManyToMany
    @JoinTable(name="Film_Categories", joinColumns=
    @JoinColumn(name="filmID", referencedColumnName="filmID"), inverseJoinColumns=
    @JoinColumn(name="categoryId", referencedColumnName="categoryId") )
    private List<FilmCategory> categoryList;



    @Override
    public String toString() {
        return "Film{" +
                "filmID=" + filmID +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", director='" + director + '\'' +
                ", productionYear=" + productionYear +
                ", timesRent=" + timesRent +
                '}';
    }
}
