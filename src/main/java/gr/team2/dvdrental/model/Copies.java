package gr.team2.dvdrental.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Copies {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private long copyID;

    @ManyToOne(optional=false)
    @JoinColumn(name="filmID",referencedColumnName="filmID")
    private Film filmID;

    @OneToOne(optional = false)
    @JoinColumn(name = "rentalID")
    private Rental rentalID;

    @Column
    private TypeOfCopy type;
    @Column
    private String comments;
    @Column
    private boolean isRent;

    @Override
    public String toString() {
        return "Copies{" +
                "copyID=" + copyID +
                ", type=" + type +
                ", isRent=" + isRent +
                '}';
    }
}
