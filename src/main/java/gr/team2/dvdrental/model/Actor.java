package gr.team2.dvdrental.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Actor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private long actorID;

    @Column(nullable = false)
    private String firstName, lastName;

    @ManyToMany(mappedBy="actorsList", fetch = FetchType.EAGER)
    private List<Film> filmList;

    @Override
    public String toString() {
        return "Actor{" +
                "actorID=" + actorID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
