package gr.team2.dvdrental.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class CustomerAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.MODULE)
    private long customerAccountID;

    @Column(nullable = false)
    private String firstName, lastName;

    @Column(unique = true, nullable = false)
    private String address;

    @Column
    private long phoneNumber;
    @Column
    private long homeNumber;

    @Column(unique = true, nullable = false)
    private String idNumber;

    @Column(unique = true, nullable = false)
    private String email;

    @OneToMany(mappedBy = "customerAccountID", targetEntity = Rental.class)
    private List<Rental> rentals;

    private double moneySpent;
    private CustomerQuality quality;

}
