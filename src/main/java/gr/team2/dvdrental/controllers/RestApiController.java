package gr.team2.dvdrental.controllers;

import gr.team2.dvdrental.model.CustomerAccount;
import gr.team2.dvdrental.model.CustomerQuality;
import gr.team2.dvdrental.model.Film;
import gr.team2.dvdrental.dtos.FilmDTO;
import gr.team2.dvdrental.service.AccountManagerImpl;
import gr.team2.dvdrental.service.FilmServiceImpl;
import gr.team2.dvdrental.util.CustomerAccountError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller //means that this class is a controller
@Slf4j
@RequestMapping(value = "/")
public class RestApiController {
    @Autowired
    AccountManagerImpl accountManagerImpl;

    // @Value("${databasePath}")
    // String path;
    /**
     * This is for testing
     * @return a string for test http response
     */
    //<--------------------JustATest------------------------------>
    @GetMapping(value="/testme")
    public ResponseEntity<String> testme(){
        String myreturn="HELLOOO TEST ME";
        return new ResponseEntity<String>(myreturn, HttpStatus.OK);
    }
    //<---------------------------------------------------------->
    //<--------------------ReturnAllCustomers------------------------------>
    /**
     *
     * @param customerAccount
     * @return a response about creating the new employee (OK or alreadyExists)
     */
    @RequestMapping(value = "/customers/{id}",method = RequestMethod.POST)
    public ResponseEntity<?> createUser (@RequestBody CustomerAccount customerAccount){
        log.info("Creating Employee: {}",customerAccount);
        CustomerAccount customer = new CustomerAccount();
        customer.setHomeNumber(2106996915);
        customer.setPhoneNumber(698573165);
        customer.setFirstName("Bob");
        customer.setLastName("o Mastoras");
        customer.setQuality(CustomerQuality.GOOD);
        customer.setAddress("Vithos tou Bikini");
        customer.setIdNumber("AH 997755");
        customer.setEmail("paixtoura@gmail.com");
        customer.setMoneySpent(0);
        log.info("This is the new customer's ID number: {}", accountManagerImpl.saveCustomerAccount(customer));
        if(accountManagerImpl.customerExists(customerAccount.getCustomerAccountID())){
            log.error(("Unable to create. An Employee with name {} already exist"),customerAccount.getFirstName());
            return new ResponseEntity(new CustomerAccountError("Customer with this id not found."), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<CustomerAccount>(customerAccount,HttpStatus.OK);
    }
    //<---------------------------------------------------------->
    //<--------------------ReturnAllCustomers------------------------------>
    /**
     * @return a list with all customers
     */
    @RequestMapping(value="/customers", method = RequestMethod.GET)
    public ResponseEntity<List<CustomerAccount>> viewAllCustomers(){
        List<CustomerAccount> customers = accountManagerImpl.getAllCustomers();
        if (customers.isEmpty()){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        log.info("These are the current customers: {}",customers);
        log.debug("Entering {}", viewAllCustomers().toString());
        log.debug("Leaving {}",viewAllCustomers().toString());
        return new ResponseEntity<List<CustomerAccount>>(accountManagerImpl.getAllCustomers(), HttpStatus.OK);
    }
    //<---------------------------------------------------------->


    @Autowired
    FilmServiceImpl filmServiceImpl;
    /**
     * @return the films list from the Json file
     */
    @RequestMapping(value = "/films",method = RequestMethod.GET)
    public ResponseEntity<List<FilmDTO>> listAllFilmsOfJsonFile(){
        List<FilmDTO> results = filmServiceImpl.viewAllFilms();
        log.debug("Response for all lists: {}",results);
        return new ResponseEntity<List<FilmDTO>>(results , HttpStatus.OK);
    }

    @RequestMapping(value = "/films/{title}",method = RequestMethod.GET)
    public ResponseEntity<Film> viewFilmByTitle(@PathVariable("title") String title){
        return new ResponseEntity<Film>(filmServiceImpl.viewFilmByTitle(title),HttpStatus.OK);
    }

    @RequestMapping(value = "/films/{id}",method =RequestMethod.GET)
    public ResponseEntity<Film> viewFilmById(@PathVariable("id") long id){
        return new ResponseEntity<Film>(filmServiceImpl.viewFilmById(id),HttpStatus.OK);
    }

    @RequestMapping(value = "/films/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUser(@PathVariable("id") long id){
        Film filmToBeDeleted  = filmServiceImpl.viewFilmById(id);
        if (filmToBeDeleted == null){
            log.error("Unable");
        }
        return null;
    }
}
