package gr.team2.dvdrental.repository;

import gr.team2.dvdrental.model.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActorsRepository extends JpaRepository<Actor,Long> {
}
