package gr.team2.dvdrental.repository;

import gr.team2.dvdrental.model.Copies;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CopiesRepository extends JpaRepository<Copies, Long> {

}
