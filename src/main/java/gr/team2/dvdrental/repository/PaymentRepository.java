package gr.team2.dvdrental.repository;

import gr.team2.dvdrental.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
