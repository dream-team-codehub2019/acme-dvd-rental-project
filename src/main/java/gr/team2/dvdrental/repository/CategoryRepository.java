package gr.team2.dvdrental.repository;

import gr.team2.dvdrental.model.FilmCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<FilmCategory,Long> {

}
