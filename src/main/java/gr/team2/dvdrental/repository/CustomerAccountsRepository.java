package gr.team2.dvdrental.repository;

import gr.team2.dvdrental.model.CustomerAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerAccountsRepository extends  JpaRepository<CustomerAccount, Long> {
    CustomerAccount findCustomerAccountByFirstName(String firstName);

}
