package gr.team2.dvdrental.repository;

import gr.team2.dvdrental.model.Rental;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RentsRepository extends JpaRepository<Rental, Long> {

}
