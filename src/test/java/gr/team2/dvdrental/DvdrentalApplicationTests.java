package gr.team2.dvdrental;

import gr.team2.dvdrental.model.*;
import gr.team2.dvdrental.service.AccountManager;
import gr.team2.dvdrental.service.RentalService;
import gr.team2.dvdrental.service.RentalServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class DvdrentalApplicationTests {
    private LocalDate today = LocalDate.now();

    @Autowired
    private AccountManager accountManager;

    @Autowired
    private RentalService rentalService;

    @Test
    public void testSaveCustomer() {
        CustomerAccount customer = new CustomerAccount();
        customer.setHomeNumber(2106996915);
        customer.setPhoneNumber(698573165);
        customer.setFirstName("Bob");
        customer.setLastName("o Mastoras");
        customer.setQuality(CustomerQuality.GOOD);
        customer.setAddress("Vithos tou Bikini");
        customer.setIdNumber("AH 997755");
        customer.setEmail("paixtoura@gmail.com");
        customer.setMoneySpent(0);
        log.info("This is the new customer's ID number: {}",accountManager.saveCustomerAccount(customer));
    }

    @Test
    public void testSaveRental(){
        CustomerAccount customer = new CustomerAccount();
        Film film = new Film();
        Copies copy = new Copies();
        Actor actor = new Actor();
        Rental rental = new Rental();
        RentalServiceImpl newRentalService = new RentalServiceImpl();

        customer.setHomeNumber(2106996915);
        customer.setPhoneNumber(698573165);
        customer.setFirstName("Bob");
        customer.setLastName("o Mastoras");
        customer.setQuality(CustomerQuality.GOOD);
        customer.setAddress("Vithos tou Bikini");
        customer.setIdNumber("AH 997755");
        customer.setEmail("paixtoura@gmail.com");
        customer.setMoneySpent(0);
        film.setTitle("Inception");
        film.setDescription("blablabla");
        film.setProductionYear(2009);
        copy.setRent(false);
        copy.setFilmID(film);
        copy.setType(TypeOfCopy.DVD);
        film.getCopies().add(copy);
        actor.setFirstName("Leo");
        actor.setLastName("DiCaprio");
        actor.getFilmList().add(film);
        film.getActorsList().add(actor);
        newRentalService.makeRental(rental, film, customer);

    }

}
